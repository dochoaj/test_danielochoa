This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## TL;DR

![TLDR](https://thumbs.gfycat.com/SoftBewitchedCirriped-max-1mb.gif)

## Description

- LocalStorage is not modified in response of an action in a reducer. Reducers should be pure, so the store is suscribed to any action and do the updating.

- LocalStorage values are JSON arrays.

- I used reselect to cook a nice data structure to render the table parsing the LocalStorage values into a hash.

- I don't put a lot of efforts in the UI but I configured a toastr element and 404 and empty pages to help a little with the UX.

## How to run

This is a create-react-app project so, the steps are pretty straightforward:

- Clone the repo

- Install dependencies using `yarn install`

- Run project with `yarn start`

- Open your browser to localhost:3000 (the browser should pop out anyway)
