import { connect } from 'react-redux';
import PresOut from './PresOut';
import { getParsedData } from './selectors';

const mapStateToProps = (state) => {
  return {
    data: getParsedData(state),
  };
};

export default connect(mapStateToProps, null)(PresOut);