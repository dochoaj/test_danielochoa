import React from 'react';
import { Link } from 'react-router-dom';

export default () => {
  return (
    <div className='jumbotron'>
      <h1 className='display-4'>These aren't the droids you're looking for</h1>
      <p className='lead'>If you're here means that you're lost, yes, absolutely lost.</p>
      <hr className='my-4' />
      <p>But we can point you out of here, please choose your destiny:</p>
      <p className='lead'>
        <Link to='/contin' className='btn btn-primary btn-lg'>
          Click here to add some data on localStorage
        </Link>
      </p>
      <p className='lead'>
        <Link to='/contout' className='btn btn-primary btn-lg'>
          Click here to view a nice table with localStorage data
        </Link>
      </p>
    </div>
  );
}