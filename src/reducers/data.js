import { ADD_DATA } from '../actions';

export default (state = {}, action) => {
  switch (action.type) {
    case ADD_DATA:
      return {
        stored_value: [...state.stored_value, action.payload.value],
        stored_selection: [...state.stored_selection, action.payload.selection],
      };
    default:
      return state;
  }
};