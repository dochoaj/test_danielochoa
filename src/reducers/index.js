import { combineReducers } from 'redux';
import dataReducer from './data';

const initialState = () => {
  return {
    data: {
      stored_value: JSON.parse(localStorage.getItem('stored_value')) || [],
      stored_selection: JSON.parse(localStorage.getItem('stored_selection')) || [],
    },
  };
};

export { initialState };
export default combineReducers({
  data: dataReducer,
});