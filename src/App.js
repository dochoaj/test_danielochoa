import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import ContIn from './ContIn';
import ContOut from './ContOut';
import NoMatch from './NoMatch';
import './styles.css';

class App extends Component {
  render() {
    return (
      <Router>
        <div className='container-fluid'>
          <Switch>
            <Route exact path='/' component={this.scopedContIn} />
            <Route exact path='/contin' component={this.scopedContIn}/>
            <Route exact path='/contout' component={this.scopedContOut}/>
            <Route component={NoMatch} />
          </Switch>
        </div>
      </Router>
    );
  }

  scopedContIn = () => {
    return <ContIn selectOptions={this.selectOptions()}/>;
  };

  scopedContOut = () => {
    return <ContOut columns={this.buildColumns()}/>
  };

  selectOptions = () => {
    return [
      { value: 'one', label: 'One' },
      { value: 'two', label: 'Two' },
      { value: 'three', label: 'Three' },
      { value: 'four', label: 'Four' },
      { value: 'five', label: 'Five' },
    ];
  }

  buildColumns = () => {
    return [
      { dataField: 'value', text: 'Value' },
      { dataField: 'selection', text: 'Selection' }
    ];
  }
}

export default App;
