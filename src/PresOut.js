import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import paginationFactory from 'react-bootstrap-table2-paginator';
import PropTypes from 'prop-types';

class PresOut extends Component {
  render() {
    return (
      <div className='presOut'>
        <div className='row justify-content-center'>
          <div className='col-8'>
            <Link to='/contin' className='btn btn-primary float-right'>
              Add data ->
            </Link>
          </div>
        </div>
        <div className='row justify-content-center'>
          <div className='col-8'>
            {this.chooseRenderMethod()}
          </div>
        </div>
      </div>
    );
  }

  chooseRenderMethod() {
    return this.props.data.length === 0 ? this.renderEmpty() : this.renderTable();
  }

  renderEmpty() {
    return (
      <div className='jumbotron'>
        <h1 className='display-4'>Well, this is logical...</h1>
        <p className='lead'>There is no data persisted on local storage, so we can't show any table here.</p>
        <p className='lead'>
          <Link to='/contin' className='btn btn-primary btn-lg'>
            Click here to add some
          </Link>
        </p>
      </div>
    );
  }

  renderTable() {
    return (
      <BootstrapTable
        keyField='id'
        data={this.props.data}
        columns={this.props.columns}
        pagination={this.buildPagination()} />
    )
  }

  buildPagination() {
    return paginationFactory({
      currentPage: this.props.currentPage,
      sizePerPageList: [ this.props.offset ]
    });
  }
}

PresOut.defaultProps = {
  currentPage: 1,
  offset: 10,
  data: [],
}

PresOut.propTypes = {
  currentPage: PropTypes.number,
  offset: PropTypes.number,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      selection: PropTypes.string.isRequired,
    })
  ),
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      dataField: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
    })
  ),
}

export default PresOut;