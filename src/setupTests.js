const mockGetItem = jest.fn()
  .mockReturnValue(JSON.stringify(''));

const localStorageMock = {
  getItem: mockGetItem,
  setItem: jest.fn(),
  clear: jest.fn()
};

global.localStorage = localStorageMock;