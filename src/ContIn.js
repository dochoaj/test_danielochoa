import { connect } from 'react-redux';
import { addData } from './actions';
import PresIn from './PresIn';

const mapDispatchToProps = (dispatch) => {
  return {
    addData: (data, selection) => {
      dispatch(addData(data, selection));
    },
  };
}

export default connect(null, mapDispatchToProps)(PresIn);