import reducer from '../reducers/data';
import * as actions from '../actions';

describe('data reducer', () => {
  it('should return the reducer initial state', () => {
    expect(reducer(undefined, {})).toEqual({});
  });

  it('should handle ADD_DATA', () => {
    const value = 'value';
    const selection = 'selection';

    expect(
      reducer(
        { stored_value: [], stored_selection: [] },
        { type: actions.ADD_DATA, payload: { value, selection }}
      )
    ).toEqual({
      stored_value: [value],
      stored_selection: [selection],
    });
  });
});