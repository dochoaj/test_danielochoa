import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import ReactTestUtils from 'react-dom/test-utils';
import PresIn from '../PresIn';

Enzyme.configure({ adapter: new Adapter() });

describe('PresIn', () => {
  let props, wrapper;

  beforeEach(() => {
    props = {
      selectOptions: [],
      addData: jest.fn(),
    };
    wrapper = shallow(<PresIn {...props} />);
  });

  it('should render correctly', () => {
    const props = {
      selectOptions: [],
      addData: jest.fn(),
    };

    const wrapper = shallow(<PresIn {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  describe('<textarea />', () => {
    it('must be defined', () => {
      expect(wrapper.find('textarea')).toBeDefined();
    });

    it('value at start should be empty', () => {
      expect(wrapper.find('textarea').props().value).toBe('');
    });

    it('must have an onChange prop', () => {
      expect(wrapper.find('textarea').props().onChange).toBeDefined();
    });

    it('onChange prop must be a function', () => {
      expect(typeof wrapper.find('textarea').props().onChange).toBe('function');
    });

    it('must update component state when it value changes', () => {
      const value = 'blah';
      const textarea = wrapper.find('textarea');
      textarea.simulate('change', {
        target: { value }
      });

      expect(wrapper.state().text).toBe(value);
    });
  });

  describe('<button />', () => {
    it('must be defined', () => {
      expect(wrapper.find('button')).toBeDefined();
    });

    it('must have an onClick prop', () => {
      expect(wrapper.find('button').props().onClick).toBeDefined();
    });

    it('onClick prop must be a function', () => {
      expect(typeof wrapper.find('button').props().onClick).toBe('function');
    });

    it('must not call parent prop addData when state is empty', () => {
      const button = wrapper.find('button');
      button.simulate('click');

      expect(wrapper.instance().props.addData).not.toBeCalled();
    });

    it('must call parent prop addData when state is filled', () => {
      wrapper.setState({ text: 'asd', selection: [{ label: 'one' }]});
      const button = wrapper.find('button');
      button.simulate('click');

      expect(wrapper.instance().props.addData).toBeCalled();
    });

    it('must call parent prop addData when state is filled', () => {
      const selection = [
        { label: 'one' },
        { label: 'two' },
        { label: 'three' },
      ];
      const text = 'asd';

      wrapper.setState({ text, selection });
      const button = wrapper.find('button');
      button.simulate('click');

      const parsedSelection = selection.map((el) => { return el.label }).join(', ');

      expect(wrapper.instance().props.addData).toBeCalledWith(text, parsedSelection);
    });
  });
});
