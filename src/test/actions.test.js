import * as actions from '../actions';
import store from '../store';

describe('actions', () => {
  it('should create a coherent action', () => {
    const value = 'value';
    const selection = 'selection';

    const expectedAction = {
      type: actions.ADD_DATA,
      payload: { value, selection},
    };

    expect(actions.addData(value, selection)).toEqual(expectedAction);
  });

  it('dispatched action should update localStorage', () => {
    const value = 'value';
    const selection = 'selection';

    store.dispatch(actions.addData(value, selection));

    expect(localStorage.setItem.mock.calls[0]).toEqual(['stored_value', JSON.stringify([value])]);
    expect(localStorage.setItem.mock.calls[1]).toEqual(['stored_selection', JSON.stringify([selection])]);
  });
});