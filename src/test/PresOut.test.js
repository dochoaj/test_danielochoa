import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import PresOut from '../PresOut';

Enzyme.configure({ adapter: new Adapter() });

describe('PresOut', () => {
  it('should render correctly', () => {
    const props = {
      columns: [],
    };

    const wrapper = shallow(<PresOut {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('chooseRenderMethod() must be defined', () => {
    const props = {
      columns: [],
      data: []
    };

    const wrapper = shallow(<PresOut {...props} />);
    expect(wrapper.instance().chooseRenderMethod).toBeDefined();
  });

  it('chooseRenderMethod() must be a function', () => {
    const props = {
      columns: [],
      data: []
    };

    const wrapper = shallow(<PresOut {...props} />);
    expect(typeof wrapper.instance().chooseRenderMethod).toBe('function');
  });

  it('should call renderEmpty() if there is no data', () => {
    const props = {
      columns: [],
      data: []
    };

    const wrapper = shallow(<PresOut {...props} />);

    wrapper.instance().renderEmpty = jest.fn();
    wrapper.update();
    wrapper.instance().chooseRenderMethod();
    expect(wrapper.instance().renderEmpty).toHaveBeenCalled();
  });

  it('should call renderTable() if there is data', () => {
    const props = {
      columns: [],
      data: [{ element: 'asd' }]
    };

    const wrapper = shallow(<PresOut {...props} />);

    wrapper.instance().renderTable = jest.fn();
    wrapper.update();
    wrapper.instance().chooseRenderMethod();
    expect(wrapper.instance().renderTable).toHaveBeenCalled();
  });
});
