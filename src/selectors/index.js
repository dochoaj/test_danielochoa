import { createSelector } from 'reselect';
import uid from 'uid';

const getStoredValue = (state) => {
  return state.data.stored_value;
};

const getStoredSelection = (state) => {
  return state.data.stored_selection;
};

export const getParsedData = createSelector(
  [ getStoredValue, getStoredSelection ],
  (storedValue, storedSelection) => {
    return storedValue.map((el, i) => {
      return { id: uid(), value: el, selection: storedSelection[i] };
    });
  },
);
