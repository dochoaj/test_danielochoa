import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Select from 'react-select';
import { ToastContainer, toast } from 'react-toastify';
import 'react-select/dist/react-select.css';
import PropTypes from 'prop-types';

class PresIn extends Component {
  state = {
    text: '',
    selection: '',
  }

  render() {
    return (
      <div className='presIn row justify-content-center'>
        <ToastContainer autoClose={1500} />
        <div className='col-8'>
          <div className='row justify-content-end'>
            <div className='col-4'>
              <Link to='/contout' className='btn btn-primary float-right'>
                View Table ->
              </Link>
            </div>
          </div>
          <form>
            <div className='form-row'>
              <div className='form-group col-12'>
                <label htmlFor='presIn-Textarea'>{this.props.valueLabel}</label>
                <textarea
                  className='form-control'
                  name='presIn-Textarea'
                  value={this.state.text}
                  onChange={this.handleTextareaChange}
                />
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-12'>
                <label htmlFor='presIn-Select'>{this.props.selectLabel}</label>
                <Select
                  multi={true}
                  name="presIn-Select"
                  value={this.state.selection}
                  onChange={this.handleSelectorChange}
                  options={this.props.selectOptions}
                />
              </div>
            </div>
            <div className='row justify-content-end'>
              <div className='col-4'>
                <button type='button' className='btn btn-success float-right' onClick={this.handleClick}>
                  {this.props.saveLabel}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }

  handleTextareaChange = (event) => {
    this.setState({ text: event.target.value });
  }

  handleSelectorChange = (selection) => {
    this.setState({ selection });
  }

  handleClick = (event) => {
    const { selection, text } = this.state;

    if (!selection || !text) {
      return toast.error('You need to fill the entire form!', {
        position: toast.POSITION.TOP_RIGHT
      });
    }

    this.setState({ text: '', selection: '' });
    const parsedSelection = selection.map((el) => { return el.label }).join(', ');
    this.props.addData(text, parsedSelection);
    toast.success('Saved to local storage', {
      position: toast.POSITION.TOP_RIGHT
    });
  }
}

PresIn.defaultProps = {
  selectOptions: [],
  valueLabel: 'Value',
  selectLabel: 'Selection',
  saveLabel: 'Save',
}

PresIn.propTypes = {
  valueLabel: PropTypes.string.isRequired,
  selectLabel: PropTypes.string.isRequired,
  saveLabel: PropTypes.string.isRequired,
  selectOptions: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ),
  addData: PropTypes.func.isRequired,
}

export default PresIn;