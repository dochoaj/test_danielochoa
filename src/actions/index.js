export const ADD_DATA = 'ADD_DATA';

export const addData = (value, selection) => {
  return {
    type: ADD_DATA,
    payload: { value, selection },
  };
};