import { createStore } from 'redux';
import rootReducer, { initialState } from './reducers';

const store = createStore(rootReducer, initialState());

store.subscribe(() => {
  const { stored_value, stored_selection } = store.getState().data;
  localStorage.setItem('stored_value', JSON.stringify(stored_value));
  localStorage.setItem('stored_selection', JSON.stringify(stored_selection));
});

export default store;